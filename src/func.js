const getSum = (str1, str2) => {
  if (( isNaN(str1) == true || isNaN(str2) == true) || (typeof str1 != 'string' || typeof str2 != 'string') )
    return false;

  let a = parseInt(str1), b = parseInt(str2);
  if ( str1 == '')
    a = 0;
  if ( str2 == '')
    b = 0;
  return (a + b).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let count_posts = 0, count_comments = 0;
  
  for(let i = 0; i < listOfPosts.length; i++){
	  if (listOfPosts[i].author == authorName)
		  count_posts++;
		  
	  if (listOfPosts[i].comments != undefined)
		  for (let j = 0; j < listOfPosts[i].comments.length; j++)
			  if (listOfPosts[i].comments[j].author == authorName)
  				count_comments++;
  }
  
  return 'Post:' + count_posts + ',comments:' + count_comments;
}

const tickets=(people)=> {
  let money = 0, ticket_cost = 25;
	
	for (let i = 0; i < people.length; i++)
		if (people[i] - ticket_cost <= money)
			money += ticket_cost;
		else
			return 'NO'
	
	return 'YES';
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
